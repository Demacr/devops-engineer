DROP TABLE IF EXISTS `domains`;
DROP TABLE IF EXISTS `records`;
DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `domains` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `name` varchar(255) NOT NULL,
  `master` varchar(128) DEFAULT NULL,
  `last_check` int(11) DEFAULT NULL,
  `type` varchar(6) NOT NULL,
  `notified_serial` int(11) DEFAULT NULL,
  `owner_id` int(4) NOT NULL
);

CREATE TABLE IF NOT EXISTS `records` (
  `id` int(11) NOT NULL PRIMARY KEY,
  `domain_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `content` varchar(64000) DEFAULT NULL,
  `ttl` int(11) DEFAULT NULL,
  `prio` int(11) DEFAULT NULL,
  `change_date` int(11) DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT '0',
  `ordername` varchar(255) DEFAULT NULL,
  `auth` tinyint(1) DEFAULT '1'
);

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(4) NOT NULL PRIMARY KEY,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `perm_templ` int(1) NOT NULL,
  `active` int(1) NOT NULL
);

INSERT INTO `domains` (`id`, `name`, `master`, `last_check`, `type`, `notified_serial`, `owner_id`)
VALUES
	(3,'handsaw-amoeba.com',NULL,NULL,'MASTER',2018081301,1),
	(4,'alligator-ancestor.com',NULL,NULL,'MASTER',2018121106,2),
	(8,'adrenaline-abomination.com',NULL,NULL,'MASTER',2018020800,1),
	(10,'curfew-alarm.com',NULL,NULL,'MASTER',2018010801,1),
	(15,'fanatic-focus.com',NULL,NULL,'MASTER',2018081304,3),
	(18,'hobby-kitten.com',NULL,NULL,'MASTER',2018041200,1),
	(19,'rude-parrot.com',NULL,NULL,'MASTER',2019051200,1),
	(20,'shake-pulse.com',NULL,NULL,'MASTER',2019051200,1),
	(24,'finger-teargas.com',NULL,NULL,'MASTER',2018012000,4);

INSERT INTO `records` (`id`, `domain_id`, `name`, `type`, `content`, `ttl`, `prio`, `change_date`, `disabled`, `ordername`, `auth`)
VALUES
	(1,3,'dread.handsaw-amoeba.com','A','10.30.4.13',1400,0,1570488695,0,NULL,1),
	(2,4,'double.alligator-ancestor.com','A','10.30.4.13',1400,0,1548947695,0,NULL,1),
	(3,8,'chameleon.adrenaline-abomination.com','A','10.30.4.13',1400,0,1553861695,0,NULL,1),
	(4,10,'feather.curfew-alarm.com','A','10.30.4.13',1400,0,1542175695,0,NULL,1),
	(5,15,'recent.fanatic-focus.com','CNAME','_self.rest-gateway.com',1400,0,1561073695,0,NULL,1),
	(6,18,'cargo.hobby-kitten.com','A','10.30.4.13',1400,0,1541650695,0,NULL,1),
	(7,24,'licker.finger-teargas.com','A','10.30.4.13',1400,0,1565489695,0,NULL,1),
	(8,3,'swollen.handsaw-amoeba.com','A','10.30.4.15',1400,0,1554316695,0,NULL,1),
	(9,4,'communion.alligator-ancestor.com','A','10.30.4.15',1400,0,1568278695,0,NULL,1),
	(10,8,'live.adrenaline-abomination.com','A','10.30.4.15',1400,0,1573305695,0,NULL,1),
	(11,10,'hill.curfew-alarm.com','A','10.30.4.15',1400,0,1541527695,0,NULL,1),
	(12,15,'hack.fanatic-focus.com','A','10.30.4.15',1400,0,1549962695,0,NULL,1),
	(13,18,'blackout.hobby-kitten.com','A','10.30.4.15',1400,0,1555243695,0,NULL,1),
	(14,24,'compound.finger-teargas.com','A','10.30.4.15',1400,0,1553521695,0,NULL,1),
	(15,3,'crabs.handsaw-amoeba.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1541785695,0,NULL,1),
	(16,4,'approaching.alligator-ancestor.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1555885695,0,NULL,1),
	(17,8,'mix.adrenaline-abomination.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1542793695,0,NULL,1),
	(18,10,'controversial.curfew-alarm.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1552968695,0,NULL,1),
	(19,15,'wall.fanatic-focus.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1547446695,0,NULL,1),
	(20,18,'terminus.hobby-kitten.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1570501695,0,NULL,1),
	(21,39,'fang.flash.meat.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1552472695,0,NULL,1),
	(22,24,'arcane.finger-teargas.com','TXT','"v=spf1 redirect=_spf.google.com"',1400,0,1552472695,0,NULL,1);

INSERT INTO `users` (`id`, `username`, `password`, `fullname`, `email`, `description`, `perm_templ`, `active`)
VALUES
	(1,'bob','1b8e8be9021ec20e61b249a2e7824d6c','Bob Roost','bob@grest-gateway.com','Administrator with full rights.',1,1),
	(2,'alice','0fac073593190bea97accb078547f556','Alice Anderson','alice@grest-gateway.com','Administrator with full rights.',1,1),
	(3,'john','7f716fd9de88e016e7f84068a05013c3','John Dew','john@grest-gateway.com','Administrator with full rights.',1,0),
	(4,'jane','0781fc12f2051f8a69978d04b295660b','Jane McWaldo','jane@grest-gateway.com','Administrator with full rights.',1,1);
