DROP VIEW IF EXISTS `records_view`;

CREATE VIEW records_view 
AS 
SELECT
    domains.id,
    domains.name,
    records.name,
    records.type,
    records.content,
    users.username,
    Cast ((
        JulianDay('now') - JulianDay(datetime(records.change_date, 'unixepoch'))
        ) As Integer)
FROM
    records 
INNER JOIN domains ON 
    domains.id = records.domain_id
LEFT JOIN users ON
    domains.owner_id = users.id
WHERE 
    records.type = 'A'
ORDER BY domains.id
;