#!/bin/bash

gzcat ../artifacts/nginx-proxy.access.log.gz | awk '{print $1;}' | sort | uniq -c | sort -r | head -15
