#!/bin/bash

getUrl() {
    status="$(curl -Is $1 -o /dev/null -w "%{http_code}\n")"
    if [ $status != "200" ]; then
        echo "$1 response: $status"
    fi
}
export -f getUrl

parallel -j10 getUrl < ../artifacts/url-list-for-querying.txt